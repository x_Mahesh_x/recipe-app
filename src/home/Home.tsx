import React from "react";
import { withRouter } from "react-router-dom";

function Home() {
  return <div>Hello World</div>;
}

export default withRouter(Home);
