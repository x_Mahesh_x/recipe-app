import React from "react";
import { IMeal } from "../Interfaces";
import { Card, CardContent, Typography } from "@material-ui/core";
import YouTube from "react-youtube";

interface MealProps {
  meal: IMeal;
}

function Meal(props: MealProps) {
  return (
    <div>
      <Card raised={true} style={{ margin: 4 }}>
        <CardContent style={{ flex: 1, padding: 16 }}>
          <Typography variant="h6" component="h4">
            {props.meal.strCategory} - {props.meal.strMeal}
          </Typography>
          {props.meal.strYoutube! ? (
            <div style={{ paddingTop: 8, paddingBottom: 8 }}>
              <YouTube opts={{ width: "100%" }} videoId={props.meal.strYoutube.split("=")[1]} />
            </div>
          ) : null}
          <Typography component="p" style={{ whiteSpace: "pre-line" }}>
            Instructions: <br />
            {props.meal.strInstructions}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}

export default Meal;
