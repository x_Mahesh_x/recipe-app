import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import { IMeals } from "../Interfaces";
import RefreshIcon from "@material-ui/icons/Refresh";
import { CircularProgress, AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import Meal from "./Meal";

function Random() {
  const [isLoading, setIsLoading] = useState(true);
  const [meals, setMeals] = useState<IMeals>();

  const fetchData = async () => {
    setIsLoading(true);
    axios
      .get(`https://www.themealdb.com/api/json/v1/1/random.php`)
      .then(result => {
        setMeals(result.data);
      })
      .catch(e => console.log(e))
      .finally(() => setIsLoading(false));
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" color="inherit" style={{ flexGrow: 1 }}>
            Recipe App
          </Typography>
          <IconButton color="inherit" onClick={fetchData}>
            <RefreshIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      {!isLoading ? (
        <div>
          {meals!.meals.map((item, i) => (
            <Meal meal={item} />
          ))}
        </div>
      ) : (
        <div style={{ textAlign: "center", paddingBottom: 16 }}>
          <CircularProgress />
        </div>
      )}
    </div>
  );
}

export default withRouter(Random);
